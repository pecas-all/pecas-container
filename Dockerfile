FROM archlinux:base-devel
MAINTAINER Nika Zhenya

# Generación de usuario
RUN useradd -m pecas && echo "pecas ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/pecas
USER pecas

WORKDIR /tmp

# Actualización de paquetes
RUN sudo pacman --noconfirm -Syu git

# Instalación de yay
RUN git clone https://aur.archlinux.org/yay-bin.git && cd yay-bin && makepkg --noconfirm --syncdeps --rmdeps --install --clean

# Instalación de paquetes
RUN yay --nodiffmenu --noeditmenu --useask --batchinstall --sudoloop --noconfirm --needed -S hunspell hunspell-en_us hunspell-es_mx jdk11-openjdk linkchecker pandoc-bin python python-setuptools ruby texlive-bin texlive-bibtexextra texlive-fontsextra texlive-latexextra tree unzip vim zip

# Instalación de gemas de Ruby
RUN gem install bibtex-ruby httparty io-console json pandoc-ruby paru ruweb && echo 'export PATH="$HOME/.local/share/gem/ruby/3.0.0/bin:$PATH"' >> $HOME/.bash_profile

# Instalación de Kindlegen
RUN curl -sL https://archive.org/download/kindlegen_linux_2.6_i386_v2_9.tar/kindlegen_linux_2.6_i386_v2_9.tar.gz | tar -xvz && sudo mv kindlegen /usr/local/bin

WORKDIR /home/pecas

# Instalación de Pecas legacy
RUN cd ~ && mkdir .pecas && cd .pecas && git clone --depth 1 https://gitlab.com/programando-libreros/herramientas/pecas-legacy.git . && bash install.sh

# Instalación de ExportPDF
RUN sudo curl -o /usr/local/bin/export-pdf https://gitlab.com/snippets/1917490/raw && sudo chmod 755 /usr/local/bin/export-pdf

# Instalación de baby-biber
RUN sudo curl -o /usr/local/bin/baby-biber https://gitlab.com/snippets/1917492/raw && sudo chmod 755 /usr/local/bin/baby-biber

# Importación de alias
RUN sudo curl https://pad.programando.li/ruweb:wiki:archlinux:config:aliases/download >> $HOME/.bash_profile
