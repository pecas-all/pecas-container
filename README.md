# Contenedor de Pecas

Este contenedor tiene las herramientas editoriales usadas por Programando LIBREros y camaradas para flujos de publicación automatizada.

Los scripts en cada contenedor ayudan a simplificar algunas tareas:

* `build`. Construye la imagen del contenedor.
* `run`. Corre una instancia del contenedor.
* `enter`. Entra a la instancia del contenedor.
* `start`. Arranca el contenedor.
* `stop`. Detiene el contenedor.
* `delete`. Elimina instancia e imagen.
* `push`. Manda la imagen a Docker Hub.
